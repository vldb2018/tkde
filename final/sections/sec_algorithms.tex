% clearly describe the execution order and resource allocation
\section{Scheduling Algorithms}
\label{sec:algorithms}
	Scheduling algorithms decide the execution order of pipelines on the global layer. In this section, we first describe three baseline algorithms, including a serialized execution, an optimal static approach and a list scheduling method. Then  we propose an optimal algorithm, List with Filling and Preemption Scheduler, based on the two techniques in Section~\ref{sec:principles}. 
	
	Before describing algorithms, we define the statuses of $P_i$ during its life cycle, which are listed below. The pipelines with the same status are stored in a sorted list ordering by its rank from higher to lower.
	\begin{enumerate}[$\bullet$]
%		\setlength{\itemsep}{1pt}
%		\setlength{\parsep}{1pt}
%		\setlength{\parskip}{1pt}
		\item Waiting (\textit{W}): $pred(P_i)$ is not empty, and $P_i$ has to wait until all its predecessors  have been finished.
		\item Ready (\textit{R}): $pred(P_i)$ is empty or any $P_j\in pred(P_i)$ has been finished. All ready pipelines are stored in a Ready List (\textit{RL}).
		\item piVot (\textit{V}): $P_i$ is running in a pivot role. All pivot pipelines are stored in a piVot List (\textit{VL}).
		\item Extra (\textit{E}): $P_i$ is running in an extra role. All extra pipelines are stored in an Extra List (\textit{EL}).
		\item Done (\textit{D}): $P_i$ has been finished.
			\end{enumerate}
		%In particular, all ready pipelines and extra pipelines are stored in $RL\cup EL$, ordering by their rank from higher to lower.

%	Correspondingly, there are some data structures storing the pipelines at a certain status.
%	\begin{enumerate}[$\bullet$]
%		\setlength{\itemsep}{1pt}
%		\setlength{\parsep}{1pt}
%		\setlength{\parskip}{1pt}
%		\item Ready List ($RL$): A set that stores ready pipelines and maintains their rank order from higher to lower.
%		\item piVot List ($VL$): A set that stores pivot pipelines.
%		\item Extra List ($EL$): A set that stores extra pipelines and maintains their rank order from higher to lower.
%		\item $RL\cup EL$: A collection of ready pipelines and extra pipelines, ordering by their rank from higher to lower.
%	\end{enumerate}
%\begin{figure}
%	\centering
%\setlength{\intextsep}{0pt} 
\SetCommentSty{itshape}
\setlength{\textfloatsep}{0pt} 
		\begin{algorithm}[t]
	\caption{LFPS algorithm (key part)}
	\LinesNumbered
	\small
	\label{alg:LFPS}
	\KwIn{\textit{RL}, \textit{EL}, \textit{VL}, $\delta$ }
	\ForEach(\Comment*[f]{schedule pivot pipelines}){$P_i \in RL\cup EL$ in order}{ \label{code:pivot1}
		%set \textit{preemption-occur} to be true\;
		${preemption} \leftarrow true$\;
		\ForEach{$P_j \in$ VL in order}{\label{code:preemption_begin}
			\eIf {$rank'(P_i) - rank'(P_j)> \delta$}{
				\If{$serv(P_i) \cap serv(P_j) \neq \emptyset $}{\label{code:conflict1}
					%set	$P_j$ to be \textit{E}, remove $P_j$ from \textit{VL} and put it into \textit{EL}\;
					$P_j$: V$\rightarrow $E\;

					break\;
				
				}
			}{
				\If{$serv(P_i) \cap serv(P_j) \neq \emptyset $}{\label{code:conflict2}
					%set \textit{preemption-occur} is false\;
						${preemption} \leftarrow false$\;
					break\;
				}
			}
		}\label{code:preemption_end}
		\If{preemption is true}{\label{code:preemption1} 
			$P_i$: R/E$\rightarrow $V\;
%			\eIf{$P_i$ is R} {
%				remove $P_i$ from \textit{RL}\;
%				send each task $T_j$ of $P_i$ to $serv(T_j)$\;
%			}{
%				remove $P_i$ from \textit{EL}\;
%			}
%			put $P_i$ into \textit{VL} and set status of $P_i$ to be \textit{V}\;
		}	\label{code:preemption2}
	} \label{code:pivot2}
	\ForEach(\Comment*[f]{ schedule extra pipelines }){$P_j \in RL$ in order}{\label{code:extra_begin}       
		\If{could schedule $P_j$}{ \label{code:extraNum}
%			set the status of $P_j$ to be \textit{E}, remove $P_j$ from \textit{RL} and put $P_j$ into \textit{EL}\;
%			send each task $T_k$ of $P_j$ to $serv(T_k)$\;
			$P_j$: R$\rightarrow $E\; 
		}
	}\label{code:extra_end}

\end{algorithm}  

%		\vspace{-15pt}
%\end{figure}

	\vspace{-4pt}
	\subsection{Baseline Algorithms}
	
	\textbf{Serialized~Scheduler~(SS)} dispatches pipelines one by one according to the physical plan with the consideration of data dependency. Thus at any time, only a pipeline is running. Each running pipeline can be accelerated by the elastic pipelining~\cite{Wang:elastic}. SS is widely adopted in traditional databases and OLAP systems.

	\textbf{Synchronized Phase (SP)}~\cite{tan1993resource} splits a PDG into shelves according to the critical path. Note that the path length is measured by the number of nodes on the path, which is different from this paper. In each shelf, pipelines without dependency are executed concurrently. But resources are allocated statically according to estimated information. This is a common way to achieve the independent parallelism in previous static approaches~\cite{garofalakis1996multi,garofalakis1997parallel}.
	
	\textbf{Simple List Scheduler (SLS)} is derived from the list scheduling algorithm \cite{graham1966bounds}, which is widely used for DAG scheduling problems~\cite{arabnejad2014list,garofalakis1996multi,garofalakis1997parallel}. The priority of every pipeline refers to its rank in Equation~(\ref{equ:rank}), reflecting the importance of the critical path. Besides, each pipeline can be executed in the elastic pipelining manner~\cite{Wang:elastic}. The steps of SLS are listed as follows.
	\begin{enumerate}[(1)]
		\item Select $P_i$ with the highest rank from \textit{RL}.
		\item Allocate servers to accommodate $P_i$.
		\item If without enough resources, the pipeline with next higher rank in \textit{RL} becomes $P_i$, then go to (2).
	\end{enumerate}
 	SLS greedily dispatches the job with higher rank according to available resources. However, it cannot meet the two challenges of SRMP as well. There are not only the three types of idle resource ``holes'', but also the two out-of-order cases. Fortunately, SLS can combine with the two techniques, adaptive filling and rank-based preemption, to solve these problems, generating \emph{List with Filling Scheduler (LFS)}, \emph{List with Preemption Scheduler (LPS)} or \emph{List with Filling and Preemption Scheduler (LFPS)}.	
	%	\begin{algorithm}[]
	%		\caption{SLS algorithm}
	%		\LinesNumbered
	%		\label{alg:SLS}
	%		\KwIn{a PDG \textit{G}}
	%		compute $rank$ for all pipelines in \textit{G}\;
	%		construct \textit{RL} with all ready pipelines in \textit{G}\;
	%		\While{RL is not empty}{
	%			\ForEach{ $P_i \in RL$  in order}{
	%				\If{resource needs of $P_i$ can be satisfied}{
	%					set the status of $P_i$ to be \textit{V}, remove $P_i$ from \textit{RL} and put $P_i$ into \textit{VL}\;
	%					send any task $T_j$ of $P_i$ to $serv(T_j)$\;
	%				}
	%			}
	%			wait any $P_j \in$ \textit{VL} to be \textit{D}\;
	%			remove $P_j$ from \textit{VL}\;
	%			\If{$P_k \in succ(P_j)$ is ready}{
	%				put $P_k$ into \textit{RL}\;
	%			}
	%		}
	%	\end{algorithm}
	%	There are two derivations based on SLS with respect to resource allocation.
	%	\begin{enumerate}[$\bullet$]
	%		\item 	{Static Simple List Scheduler (S-SLS):} The resource needs of pipeline are estimated according to its workload at query compile time, mainly focusing on CPU cores and supposing memory is large enough. If the resources are allocated to one task, then they can not be shared with other tasks, similar to traditional static resource reservation.
	%		\item  	{Elastic pipelining Simple List Scheduler (E-SLS):} We take the coarse grained resource allocation strategy to allocate servers, and each pipeline is also executed in the Elastic Pipelining manner. This algorithm is superior to the Serialized Scheduler when there are less resource conflicts among pipelines, conversely, to extreme case, E-SLS degenerates to SS.
	%	\end{enumerate}
	%
	% \subsection{Greedy Scheduler (GS)}
	% There is one greedy idea to issue all ready pipelines due to the pipeline is elastic, in other words, all ready pipelines are in extra execution manner. In order to avoid running too many pipelines at the same time, some limitations are added.
	% \begin{algorithm}
	% 	\caption{GS algorithm}
	% 	\LinesNumbered
	% 	\label{alg:GS}
	% 	\KwIn{a PDG \textit{G}}
	% 	compute $rank$ for all pipelines in \textit{G}\;
	% 	construct \textit{RL} with all ready pipelines in \textit{G}\;
	% 	\While{RL is not empty}{
	% 		\ForEach{ $P_i \in$ RL in order}{
	% 			\If{$P_i$ could be issued}{
	% 				set the status of $P_i$ to be \textit{V} and put $P_i$ into \textit{EL}\;
	% 				send plan fragment of $P_i$ to each $serv_k \in serv(P_i)$\;
	% 				remove $P_i$ from \textit{RL}\;	
	% 			}
	% 		}
	% 		wait any $P_j \in EL$ to be \textit{Done}\;
	% 	}
	% \end{algorithm}
	%	
	%	
	%	\subsection{List with Filling Scheduler (LFS)}
	%	List with Filling Scheduler is derived from E-SLS. After each scheduling time of pivot pipelines, it issues some extra ready pipelines to fill idle resource. In case of issuing too many extra pipelines to bring much scheduling overhead, according to the experiment in Section~\ref{sec:extra pipeline number}, we limit at most five extra pipelines on each server, this is checked at Line \ref{code:check} in Algorithm \ref{alg:LFS}.
	%	\begin{algorithm}[]
	%		\caption{LFS algorithm}
	%		\LinesNumbered
	%		\label{alg:LFS}
	%		\KwIn{a PDG \textit{G}}
	%		compute $rank$ for all pipelines in \textit{G}\;
	%		construct \textit{RL} with all ready pipelines in \textit{G}\;
	%		\While{$RL \cup EL$ is not empty}{
	%			$//$scheduling pivot pipelines\\
	%			\ForEach{$P_i \in RL \cup EL$ in order}{
	%				\If{resource needs of $P_i$ can be satisfied}{
	%					set the status of $P_i$ to be \textit{V}, remove $P_i$ from \textit{RL} or \textit{EL} and put $P_i$ into \textit{VL}\;
	%					send any task $T_j$ of $P_i$ to $serv(T_j)$\;
	%				}
	%			}
	%			$//$scheduling extra pipelines \\
	%			\ForEach{$P_j \in RL$  in order}{
	%				\If{could schedule $P_j$}{ \label{code:check}
	%					set the status of $P_j$ to be \textit{E}, remove $P_j$ from \textit{RL} and put $P_j$ into \textit{EL}\;
	%					send any task $T_k$ of $P_j$ to $serv(T_k)$\;
	%				}
	%			}
	%			
	%			wait any $ P_k \in VL \cup EL $ to be \textit{D}\;
	%			remove $P_k$ from \textit{VL} or \textit{EL}\;
	%			\If{$P_i \in succ(P_k)$ is ready}{put $P_i$ into \textit{RL}\;
	%			}
	%		}
	%		
	%	\end{algorithm}
	
	\vspace{-8pt}
	\subsection{List with Filling and Preemption Scheduler}
	List with Filling and Preemption Scheduler (LFPS) is derived from SLS. It not only combines with the adaptive filling to fully use the idle resource ``holes'' but also incorporates the rank-based preemption to guarantee an optimal execution order of pipelines. Furthermore, it derives \emph{Event-Driven LFPS (ED-LFPS}) and \emph{Time-Driven LFPS (TD-LFPS)}, according to the two types preemption, namely event-driven preemption and time-driven preemption. They invoke Algorithm \ref{alg:LFPS} to schedule pipelines. %Particularly, since allocated resources of a pipeline can be coordinated effectively among tasks \cite{Wang:elastic}, we allocate resources to pipelines in a coarse manner in LFPS. We take one server, instead of one core, as an allocation unit on the global layer. In other words, one server is only allocated to a pivot pipeline. We will discuss this later in Section~\ref{sec:discussion}.
	
	Algorithm \ref{alg:LFPS} describes LFPS in detail. First, pivot pipelines are scheduled according to SLS with preemption (lines \ref{code:pivot1}-\ref{code:pivot2}). Then, extra pipelines are issued to fill idle resources (lines \ref{code:extra_begin}-\ref{code:extra_end}). But we do not dispatch too many extra pipelines (line \ref{code:extraNum}). To issue enough extra pipelines but avoid too much scheduling overhead, we generally arrange at most 4 extra pipelines on each server, which is verified through experiments. As regard to preemption, if conditions are satisfied (lines \ref{code:preemption_begin}-\ref{code:preemption_end}), LFPS sends preemption commands to each server and changes the status of tasks (lines \ref{code:preemption1}-\ref{code:preemption2}). In particular, resource conflicts between two pipelines are judged by whether their required servers are overlapped or not (lines \ref{code:conflict1}, \ref{code:conflict2}). Because we allocate resources to a pipeline in a coarse manner, taking a server, instead of a core as an allocation unit. The allocated servers can be efficiently used by pipelines in our work. As for $\delta$, it will be analyzed through the experiments in Section~\ref{sec:overhead}. %After a pipeline is completed, if its results differ prior estimations seriously, we update the cost of waiting  and ready pipelines in PDG.
	%After received preemption commands on each related slave server, TM changes the status of tasks, then resources will be shifted to important tasks under RC. %This detail is described in Section~\ref{sec:coordinator}.

The complexity of Algorithm 1 is $ O(vm) $, where $ v $ is number of pipelines and $ m $ is the number of servers in a cluster. $ m $ stems from the fact that there is at most one pivot pipeline on a server. Since ED-LFPS invokes Algorithm 1 when a pipeline is ready at run time, the complexity of ED-LFPS is $O(v^{2}m)$. It is the same as that of some acceptable static list scheduling algorithms~\cite{topcuoglu2002performance,arabnejad2014list}. While TD-LFPS invokes Algorithm 1 every $ \tau $ time, its complexity is $ O(vmc) $, where $ c $ is the invoking counts. Generally, if the cost time of Algorithm 1 is less than $ \tau $ to some extent, the scheduling overhead cannot be a bottleneck.

%	From the perspective of each pipeline, the changing of its status can be expressed as a state machine in Fig. \ref{fig:stateMachine}.
%		\\ $W->R: $ the default status of each $P_i$ is \textbf{W}aiting, once all pipelines in $pred(P_i)$ are finished or $pred(P_i)$ is empty, then the status of $P_i$ is changed to be \textbf{R}eady.
%		\\ $R->V: $ when there are enough resources for $P_i$, then the status of \textbf{R}eady pipeline $P_i$ is updated to pi\textbf{V}ot at the beginning of running.
%		\\ $R->E: $ when \textbf{R}eady pipeline $P_i$ starts to execute in extra manner, then its status is shifted to \textbf{E}xtra.
%		\\ $E->V: $ when extra pipeline $P_i$ gets preference from one pivot pipeline.
%		\\ $V->E: $ when pivot pipeline $P_i$ is preempted by other extra pipelines.
%		\\ $E->D: $ \textbf{E}xtra pipeline is \textbf{D}one.
%		\\ $V->D: $ pi\textbf{V}ot pipeline is \textbf{D}one.		
%	\begin{figure}
%		\centering
%		\includegraphics[width=1.7in,height=0.7in]{../fig/stateMachine}
%		\caption{State machine of pipeline status}
%		\label{fig:stateMachine}
%	\end{figure}


%\vspace{-10pt}

	From the perspective of each pipeline, the transition of its status can be expressed as a state machine in Fig. \ref{fig:stateMachine}.
	
		\begin{enumerate}[$\bullet$]
		\setlength{\itemsep}{1pt}
		\setlength{\parsep}{1pt}
		\setlength{\parskip}{1pt}
			\item W$\rightarrow $R: The default status of each $P_i$ is \textbf{W}aiting. Once all pipelines in $pred(P_i)$ are finished or $pred(P_i)$ is empty, then the status of $P_i$ is changed to be \textbf{R}eady.
			\item R$\rightarrow $V: If there are enough resources for $P_i$, then the status of a \textbf{R}eady pipeline $P_i$ is updated to be pi\textbf{V}ot, and its tasks are sent to corresponding servers to run.
			\item R$\rightarrow $E: When a \textbf{R}eady pipeline $P_i$ starts to execute in an extra role, then its status is shifted to be \textbf{E}xtra, and its tasks are sent to corresponding servers to run.
			\item E$\rightarrow  $V: When an \textbf{E}xtra pipeline $P_i$ gets preference from a pivot pipeline, its role is changed to be \textbf{P}ivot.
		\item V$\rightarrow $E: When a \textbf{P}ivot pipeline $P_i$ is preempted by other  pipelines, its role is transformed to be \textbf{E}xtra.
			\item E$\rightarrow $D: An \textbf{E}xtra pipeline is \textbf{D}one.
			\item V$\rightarrow $D: A pi\textbf{V}ot pipeline is \textbf{D}one.	
		\end{enumerate}
	
	\begin{figure}
			\begin{minipage}[t]{0.48\linewidth}
		\centering
	\includegraphics[width=3cm,height=2.8cm]{../fig/stateMachine}
		\caption{State machine of a pipeline}
		\label{fig:stateMachine}
	\end{minipage}
	\hfill
	\begin{minipage}[t]{0.5\linewidth}
			\centering
		\includegraphics[width=4.5cm,height=2.8cm]{../fig/RA}
		\caption{Resource coordinator}
		\label{fig:RA}
	\end{minipage}

	\end{figure}

	%Similar to LFPS, SLS can couple with the adaptive filling or rank-based preemption, generating \emph{List with Filling Scheduler (LFS)} or \emph{List with Preemption Scheduler (LPS)}, respectively.
%\setlength{\textfloatsep}{15pt} 