	\section{parallelizing multiple pipelines}
\label{sec:parallelize}
	%The \emph{two-phase} optimization technique is widely used in parallel databases~\cite{tan1993resource}. The first phase generates an optimized parallel physical plan. The second phase  optimally schedules resources to minimize the completion time of the physical plan, known as \emph{resource scheduling}, which is the focus of this paper. Here, we assume an optimal physical plan is generated for any query from the first phase, and the operator cost is estimated in a query optimizer. These are widely studied in the literature~\cite{chen1992using, liu2005revisiting,GridPartition}, but beyond the scope of this paper. 

	In a main memory database cluster, each physical machine, called a server, is equipped with large memory, multi-core processors and interconnected by high-speed network. In particular, large memory could accommodate all tables and intermediate results to avoid costly disk I/O. As regard to scheduling cluster resources for query evaluation, just taking the pipelined parallelism and partitioned parallelism is not enough to fully utilize resources. From the perspective of the independent parallelism, parallelizing multiple pipelines could provide more chance to make better use of resources. In this section, we first present some attributes of a PDG, and their notations are listed in Table~\ref{tab:notations formulation}. Next, we elaborate the problem of scheduling resources to multiple pipelines in detail. Finally, we present an overview of our solution to this problem.

	\subsection{Attributes of a PDG}	
	Any physical plan can be described as a PDG, $G=(V,E)$, where $V$ is the set of pipelines and $E$ is the set of edges among pipelines. Some attributes about the PDG are defined below, which will be referred to later.
		
	\textbf{Entry \& Exit Pipeline.} Each directed edge $\left\langle P_i,P_j \right\rangle\in E$ represents the data dependency such that the pipeline $P_j$ can only be executed after the completion of the pipeline $P_i$. For any given $\left\langle P_i,P_j \right\rangle$, $P_i$ is called an immediate \emph{predecessor} of $P_j$, and  $P_j$ is an immediate \emph{successor} of $P_i$. The set of predecessors of $P_i$ is denoted as $pred(P_i)$. Especially, if $pred(P_i)=\emptyset$, $P_i$ is called an \emph{entry pipeline},  defined as $P_{entry}$. 
    Accordingly, the set of successor pipelines of $P_i$ is denoted as $succ(P_i)$. If $succ(P_i)=\emptyset$, then $P_i$ is an \emph{exit pipeline}, defined as $P_{exit}$. Except for $P_{exit}$, any pipeline only has one immediate successor, i.e.,  $|succ(P_i)|=1$, due to the tree structure of the PDG.
	
	%TODO(related to experiments, clarify the elasped time of queries or pipelines)
	%	\textbf{Makespan.} Makespan~\cite{arabnejad2014list,topcuoglu2002performance} denotes the \emph{finish time (FT)} of the exit pipeline in the scheduled PDG and is defined by
	%	\begin{equation}
	%	\label{equ:FT}
	%	makespan = FT(P_{exit})
	%	\end{equation}
	
	
	\textbf{Pipeline Width.} In a cluster, any task $T_i$ of a pipeline is dispatched to a destination server, called $serv(T_i)$. The set of servers, where any task $T_i$ of $P_j$ runs, is defined as $serv(P_j)$, namely $serv(P_j)=\cup serv(T_i)$. The number of servers in $serv(P_j)$, i.e., $|serv(P_j)|$, is denoted as \emph{pipeline width (PW)}. In particular, if $serv(P_k) \cap serv(P_j) \neq \emptyset$, then there are \emph{resource conflicts} between $P_k$ and $ P_j $. Deciding an optimal $serv(P_j)$ for each pipeline $ P_j $ is also an intractable problem, which is the focus of the partitioned (horizontal) parallelism~\cite{GridPartition,Wilschut1992Parallelism}. According to those work, $serv(P_j)$ is determined at compile time with the consideration of data locality in this paper.

	\textbf{Cost of Pipelines.} To fairly compare the cost of different pipelines, we express the average cumulative sum of each operator $ O_j $ of $P_i$ as the cost of $P_i$. If $ P_i $ has $ m $ operators, $ cost(P_i)$ is given by
	\begin{equation}
	cost(P_i) = \sum_{j=1}^{m}{cost(O_j)}/ PW(P_i),O_j \in P_i  
	\end{equation}
	This definition is an adaption of the classic
	cost model~\cite{kossmann2000state} to estimate the cost of a plan. The model first estimates the cost of every individual operator of the plan and then sums up these costs. Particularly, the cost of an operator is composed of CPU cost ($ C_{{CPU}} $) and network cost ($ C_{{NET}} $), i.e., $cost(O_j) = \lambda C_{{CPU}} + C_{{NET}} $, where $\lambda$ depends on how much slower is the network speed than the CPU speed. 
	
% 	 All tasks of $P_j$ are executed simultaneously across all servers in $serv(P_j)$ to achieve efficient pipelining and avoid data materialization.
 	 
 	 
 	 \begin{table}
 	 	\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
 	 	\centering
 	 	\caption{Notations about Problem Formulation}
 	 	\blankminipage
 	 	\begin{tabular}{rl}\toprule
 	 		{Notation} & {Semantics}  \\
 	 		\midrule
 	 		$O_i$ & a physical Operator	\\				
 	 		$P_i$ & a Pipeline	\\	
 	 		\emph{$T_i$} & a Task \\ 
 	 		$ pred(P_i) $ & the set of immediate predecessors of $P_i$   \\	
 	 		$ succ(P_i) $ & the set of immediate successors of $P_i$   \\	
 	 		$P_{entry}$ & entry pipeline \\	 %, $pred(P_{entry}) =\emptyset $
 	 		$P_{exit}$ & exit pipeline  \\	%	, $succ(P_{exit})= \emptyset$
 	 		$ cost(O_i) $ & the cost of $O_i$  \\	
 	 		$ cost(P_i) $ & \tabincell{l}{the average cumulative sum of all operators in $P_i$}  \\	
 	 		$ rank(P_i) $ & \tabincell{l}{the cumulative cost of pipelines on the path\\ from $P_i$ to the exit pipeline}  \\	
 	 		\emph{CP} & Critical Path  \\	
 	 		\emph{PW} & Pipeline Width  \\	
 	 		%	$serv_i$ & one server	\\	
 	 		\emph{$serv(T_i)$} & the server where task $T_i$ runs\\	
 	 		\emph{$serv(P_i)$} & $\cup serv(T_j)$, where $T_j$ is a task of $P_i$  \\	
 	 		%\emph{FT} & Finish Time \\ 
 	 		\bottomrule
 	 	\end{tabular}%
 	 	\label{tab:notations formulation}%
 	 	\vspace{-13pt}
 	 \end{table}%
 	 
\textbf{Priority of Pipelines.}
 	We define $rank(P_i)$ to quantify the priority of $P_i$, which is the cumulative cost of pipelines on the path from $P_i$ to $P_{exit}$, including $P_i$ but excluding $P_{exit}$, because $P_{exit}$ cannot parallelize with others. Except for $P_{exit}$, any other pipeline only has one successor pipeline, thus $rank(P_i)$ is defined by
 	\begin{equation}
 	\label{equ:rank}
 	rank(P_i) = rank(succ(P_i)) + cost(P_i) 
 	\end{equation}
 	%	where $cost(P_i)$ can be inherited from the query optimizer. However, the $cost(P_i)$ may be not accurate due to uncertain information at compile time, resulting in a suboptimal CP. To alleviate this, once $P_i$ is finished, the cost of any $P_j$ on the path from the $P_i$ to the $P_{exit}$ is reestimated, then CP may be changed. For the example in  Fig.~\ref{fig:sql3PhysicalPlan}, after $P_1$ finished, the cardinality of hash table in build(O) is certain, then the estimated join results of build(O) and probe(O) would be more precise.
 	Since the rank is computed recursively by traversing a PDG upward, starting from the side of the exit pipeline, it is known as upward rank~\cite{topcuoglu2002performance}.	
 	The rationality of $rank(P_i)$ refers to the remaining work to do on the path from $P_i$ to  $P_{exit}$ in a PDG.
 	
 	\textbf{Critical Path.} %Given a PDG, the longest path from any  pipeline to the exit pipeline is defined as a \emph{Critical Path (CP)}. It is computed by considering the cost of pipelines on the critical path.
    If $rank(P_i)$ is the largest value among all parallelizable pipelines' rank, then the path from $P_i$ to $P_{exit}$ is a \emph{critical path (CP)} of a PDG. It determines the lower bound of the PDG's finish time~\cite{kwok1996dynamic}. To this end, scheduling pipelines according to the order of $rank(P_i)$ can keep the preference of pipelines on a CP so that to minimize the finish time of a PDG.
 	
 


%\textbf{The following sentence is not clear: }The CP of a PDG potentially determine the schedule length, due to the cumulative cost of pipeline on CP is the lower bound on the schedule length~\cite{kwok1996dynamic}.
	
 	
 	%Even worse, more resources may be wasted when taking independent parallelism to parallelize multiple pipelines in a PDG. As analyzed in the introduction section, making accurate resource allocation is impossible due to uncertain information at compile time and dynamic data flow. So it is necessary to take some other efficient methods to improve resource utilization. Furthermore, 
 		\vspace{-10pt}
 	\subsection{Problem Statement}
 	In a main memory database cluster, any SQL query is first converted into a single-server plan. Then this plan is transformed into a multi-server plan, in order to reduce the search space. This approach is commonly adopted in distributed query processing~\cite{kossmann2000state}. The multi-server plan includes not only its evaluation cost but also the destination servers for each pipeline. Because the locations of evaluation also decide the shape and cost of plans. For example, a multi-server plan should add an exchange operate if the needed data is on remote servers, additionally increasing the evaluation cost. Optimizing multi-server plans is widely studied in the literature~\cite{GridPartition,kossmann2000state,parallelplan}, but beyond the scope of this paper. It belongs to the first phase of the \emph{two-phase} optimization technique~\cite{tan1993resource}. Here, we focus on the second phase, resource scheduling.
 	
 	Given a multi-server plan, its PDG and destination servers of each pipeline refer to the data dependency and resource conflicts among pipelines, respectively. They are two \emph{constrains} to schedule multiple pipelines of a query.
    Specifically, $P_i$ can be \emph{ready} to run after all its predecessors have been finished. Even though two ready pipelines do not have data dependency, they may compete for the same resources. In this case, allocating resources to which of them first is a tradeoff such that different execution order may cost different time to complete them. Therefore, there are no data dependency and resource conflicts among simultaneously running pipelines. But these running pipelines may seriously waste resources, which is another challenging problem as analyzed in the introduction section.
    
    %and their finish time cannot be predicated. In addition, making accurate resource allocation statically is impossible due to uncertain information at compile time and dynamic data flow. So it is necessary to take dynamic methods to improve resource utilization for running pipelines. These are the two key \emph{challenges}, as analyzed in the introduction section, to schedule multiple pipelines of a query.
 	
 	%The response time or \emph{makespan} of a PDG equals the finish time of its exit pipeline, as shown in Equation~\ref{equ:FT}. Therefore, it is necessary to take dynamic strategies to schedule multiple pipelines with data dependency and resource conflicts.



	
	%Specifically, in a PDG, one pipeline can be ready to  execute once its predecessor pipelines have been completed, and it can also executed concurrently with others, if they do not have data dependency. The tasks of one pipeline are dispatched to several destination servers where the corresponding data partition located for data locality, rather than moving data. In other words, the task location is decided by data location. Besides, these tasks are executed simultaneously to achieve efficient pipelining and avoid data materialization. Note that the task is preemptive, due to it is executed in elastic~\cite{Wang:elastic,leis2014morsel} in this paper.
	
	
	The \emph{objective} of Scheduling Resources to Multiple Pipelines of one query in a memory database cluster (SRMP) problem is to dynamically decide the execution order of pipelines from a given PDG and improve resource utilization of running pipelines such that the finish time of the query is minimized.	
	
	%Parallelizing Multiple Pipelines in Cluster (PMPQC): In main memory database cluster, how to parallelize multiple pipelines of one PDG from query physical plan to minimize the response time (makespan) of query. Generating optimal physical plan is extensively studied in literature~\cite{chen1992using, liu2005revisiting}, but it is out scope of this paper. We assume it given from query optimizer. 	PMPQC is NP-hard~\cite{garofalakis1996multi} and have some distinctive points as follows.
%	\begin{enumerate}[$\bullet$]
	%	\item pipeline execution is distributed and parallel: In partitioned parallelism, tasks of one pipeline are dispatched to several destination servers where data located, rather than moving data in main memory database cluster for data locality, and they are executed simultaneously for efficient pipelining and avoiding data materialization. Therefore, the servers on which pipelines of PDG will run is determinate by data partition location. %Data repartition is often issued, then production and consumption relations exist among tasks, specially, one consumer task and one producer task which belong to one pipeline would be on the same server.
		
		%\item execution instance of pipelined pipeline based on EP is moldable and even could be sleep: The number of running thread for each task can be modified at runtime and even be shrunk to 0, achieves sleep mode.
		
	%	\item multiple pipelines with DAG dependency: After one pipeline is finished, its results may be the data source of another pipeline, the data dependency among pipelines is shown as a PDG. Specially, the PDG of one query is a tree-structure graph, because the results of one pipeline could be the data source for at most one another pipeline.
		
		%\item NP-complete: PMPQC can be reduced to DAG scheduling on multiprocessors problem \cite{topcuoglu2002performance,arabnejad2014list,kwok1996dynamic}, which has been proven to be NP-complete  \cite{garey:computers,lewis:introduction}.
		
%	\end{enumerate}
	
	%Because of the complexity of PMPQC, predicting execution time or resource requirements of pipeline at compile time is intractable, so we pay attention to scheduling pipelines at run time.
		\begin{figure}
		\centering
		\includegraphics[width=5.5cm,height=2.4cm]{../fig/framework}
		\caption{The two-layer scheduling framework}
		\label{fig:Scheduling framework}
		\vspace{-15pt}
	\end{figure}

		\vspace{-10pt}
	\subsection{Overview of Our Solution}
	SRMP can be reduced to the DAG scheduling on multiprocessors problem, which has been proven to be NP-complete~\cite{topcuoglu2002performance,arabnejad2014list,kwok1996dynamic}. To deal with the two challenges of SRMP, we first present two dynamic techniques in Section~\ref{sec:principles} to improve resource utilization and determinate an optimal execution order. Based on them, we  design a two-layer scheduling framework shown in Fig.~\ref{fig:Scheduling framework}.
	\begin{enumerate}[$\bullet$]
		\setlength{\itemsep}{1pt}
		\setlength{\parsep}{1pt}
		\setlength{\parskip}{1pt}
		\item On the global layer, a \emph{Pipeline Scheduler (PS)} generates a PDG from a physical plan. Based on current available resources, the PS dynamically makes scheduling decisions according to the scheduling algorithms in Section~\ref{sec:algorithms}.
		\item On the local layer, resources of each server are coordinated efficiently among tasks by a \emph{Resource Coordinator (RC)} in Section~\ref{sec:coordinator}. To confirm the efficiency of CPU cores, the RC takes the scalability bound into consideration to avoid allocating excessive cores to a task. To improve resource utilization, the RC schedules extra tasks to adaptively fill resource ``holes'' from other tasks.
		%TODO(describe more details after 6)
    \end{enumerate}
	%In the framework, scheduling decisions are made globally according to scheduling algorithms in Section~\ref{sec:algorithms}. On each slave server, local resources are coordinated efficiently among tasks in Section~\ref{sec:coordinator}. The scheduling framework is shown in Fig. \ref{fig:Scheduling framework}.
	%\begin{enumerate}[$\bullet$]
	%	\item Pipeline Scheduler (PS): On master, it extracts a PDG from one given physical plan, schedules pipelines according to scheduling algorithms and receives control message from scheduled pipelines, such as the changes of task status, heartbeats for failure detection, etc.
	%	\item Task Manager (TM): On slave, it receives tasks and the preemption command from PS and reports the task execution status and heartbeats to the PS.
	%	\item Resource Coordinator (RC): On slave, it perceives the computing resource requirements of different tasks and allocates resources to tasks adaptively.
	%\end{enumerate}

	
	%The life cycle of one pipeline contains the following steps:
	%\begin{enumerate}[(1)]
	%	\item After $P_i$ is decided to be run by PS, any task $T_j$ of which is sent to the $serv(T_j)$.
	%	\item The TM on $serv(T_j)$ receives $T_j$, then $T_j$ begins to execute, asks resource from RC and reports status to PS until completed.
	%\end{enumerate}